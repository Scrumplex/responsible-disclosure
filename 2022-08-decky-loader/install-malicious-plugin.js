// Decky requests user interaction if something requests the installation of a plugin.
// By overriding the following function, we are able to skip the user interaction part and install plugins unattendedly.
DeckyPluginLoader.addPluginInstallPrompt = (artifact, version, request_id, hash) => {
    DeckyPluginLoader.callServerMethod("confirm_plugin_install", { request_id });
};
(() => {
    const formData = new FormData();
    // it is important, that the name of the plugin matches the directory name inside the zip file.
    formData.append("name", "vibrantDeck");
    formData.append("artifact", "https://cdn.tzatzikiweeb.moe/file/steam-deck-homebrew/versions/c72e25f281f9ac67dbfdbfddb9b6ffdc47e8a0be167de8d0e9738ea5bc6f4157.zip");
    formData.append("version", "1.0.0");
    formData.append("hash", "c72e25f281f9ac67dbfdbfddb9b6ffdc47e8a0be167de8d0e9738ea5bc6f4157");
    fetch("http://localhost:1337/browser/install_plugin", {
        method: "POST",
        body: formData
    });
})();
