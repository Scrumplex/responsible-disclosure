# Steam Deck Homebrew - Decky Loader
Remote Code Execution vulnerability writeup

Affected versions: \<v2.0.4-198591d-pre
Fixed version: \>=v2.0.4-198591d-pre

## Timeline
All timestamps are in UTC

- 2022-08-04 - Discovered potential security issue with the RPC implementation of Decky Loader
- 2022-08-05 - Found a Remote Code Execution (RCE) vulnerability using Cross-Site-Request-Forgery (CSRF)
- 2022-08-05 - Privately disclosed vulnerability to developers
- 2022-08-06 - New version of Decky Loader released that implements CSRF protections

## Details
Decky Loader provides a HTTP service (listening on `127.0.0.1:1337` by default), which is used for communication between the frontend component running in a Chromium Embedded Framework (CEF) environment and the backend which is a Python daemon.
By its nature, the HTTP service can be accessed by any program running on the computer, including web pages in a web browser.
If an attacker forges a malicious web page, that communicates with that HTTP service, it is possible to achieve a RCE on the victim's computer.
An attacker can inject JavaScript code into the WebView of CEF, which can then in turn be used to install a plugin into Decky Loader, that can potentially execute processes with root privileges.

The attack exploits the fact that the HTTP service does not have any CSRF protections.
By forging a `POST` request using a HTML `form` element, an attacker is able to communicate with the HTTP service, without needing any user interaction, after the user has clicked a malicious link.

The HTTP service requires a request body in the JSON format.
HTML5 does not offer any built-in way to transmit JSON bodies using HTML `form` elements.
We can however exploit the flexibility of HTML `form` elements to forge valid JSON only using HTML5 components.

Example:

```html
<form action="https://example.com" method="POST" enctype="text/plain">
    <input name='{"action":"do_something","garbage":"' value='"}'>
</form>
```

When submitted (can be done without user interaction using JavaScript), this will send the following HTTP request:
```
POST / HTTP/1.1
Content-Type: text/plain
Content-Length: 39

{"action":"do_something","garbage":"="}
```
Notably the `Content-Type` header is set to `text/plain` and the request body is *not* URL-encoded.
The `garbage` key-value pair, is the result of the HTML `form` elements' encoding of the `input` elements.

Decky Loader assumes that the content is valid JSON (no matter the `Content-Type` header) and just ignores the body if it is not parsable.
By combining this HTML `form` hack with the unprotected endpoints of Decky Loader, an attacker is able to run any action.

## Proof of Concept
Proof of concept code is provided at [2022-08-decky-loader/](2022-08-decky-loader).
These files can be served using a HTTP server.
The attacker would just need to send a direct link to one of the provided `.html` files, for example using the chat functionality of the system.
The victim just needs to click on the link, while Decky Loader is running.
